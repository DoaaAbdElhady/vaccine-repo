## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Vaccination Project is a project to controlreservation and approve vaccination.
	
## Technologies
Project is created with:
* Java 8
* Spring Boot 2.4.2 
* Angular 11
* Maven
* MySQL DB
	
## To run this project:

```
$ cd ../vaccinationmanager/target
$ java -jar vaccinationmanager-0.0.1-SNAPSHOT.jar
$ cd ../VaccinationManagerApp
$ ng serve
```
update the ip if not local host in file   ../VaccinationManagerApp\src\environments\environment.ts
To access the running app: http://localhost:4200/

## Features
* Vaccine Types including information(name, branches, no. of dosaage, expiration period, side effects, storage condition).
* Branches including information(name, phone, address, email, working days, working hours, vaccines available in this branch).
* Request vaccination including(Personal information 'name, email, phone, address', Reservation Date, Reservation Time, Payment Method).
* Send confirmation email after request vaccination.
* Pending vaccines including all pending requests and can approve vaccination done form this page.
* Send confirmation email after approving vaccination.
* Applied vacination by branch report
* Vaccination by status, date and date period reports. 

