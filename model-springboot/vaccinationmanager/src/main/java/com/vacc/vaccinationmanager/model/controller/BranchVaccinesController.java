package com.vacc.vaccinationmanager.model.controller;

import com.vacc.vaccinationmanager.model.entity.BranchVaccines;
import com.vacc.vaccinationmanager.model.service.BranchVaccinesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branchesvaccines")
public class BranchVaccinesController {
    private final BranchVaccinesService branchVaccinesService;


    public BranchVaccinesController(BranchVaccinesService branchVaccinesService) {
        this.branchVaccinesService = branchVaccinesService;
    }


    @GetMapping("/findAll")
    public ResponseEntity<List<BranchVaccines>> findAll()
    {
        List<BranchVaccines> branchesList= branchVaccinesService.findAll();
        return new ResponseEntity<>(branchesList, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<BranchVaccines> findBranchVaccinesById(@PathVariable("id") Long id)
    {
        BranchVaccines branches= branchVaccinesService.findBranchVaccineById(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }


    @GetMapping("/findBranchVaccineByStatus/{statusId}")
    public ResponseEntity<List<BranchVaccines>> findBranchVaccineByStatus(@PathVariable("statusId") Long id)
    {
        List<BranchVaccines> branches= branchVaccinesService.findBranchVaccineByStatus(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
    @GetMapping("/findBranchVaccineByBranchId/{branchId}")
        public ResponseEntity<List<BranchVaccines>> findBranchVaccineByBranchId(@PathVariable("branchId") Long id)
    {
        List<BranchVaccines> branches= branchVaccinesService.findBranchVaccineByBranchId(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
    @GetMapping("/findBranchVaccineByVaccineId/{vaccineId}")
    public ResponseEntity<List<BranchVaccines>> findBranchVaccineByVaccineId(@PathVariable("vaccineId") Long id)
    {
        List<BranchVaccines> branches= branchVaccinesService.findBranchVaccineByVaccineId(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<BranchVaccines> saveBranchVaccines(@RequestBody BranchVaccines branches)
    {
        BranchVaccines newBranchVaccines= branchVaccinesService.saveBranch(branches);
        return new ResponseEntity<>(newBranchVaccines, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> deleteBranchVaccines(@PathVariable("id") Long id)
    {
        branchVaccinesService.deleteBranchVaccine(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
