package com.vacc.vaccinationmanager.model.controller;

import com.vacc.vaccinationmanager.model.entity.Branches;
import com.vacc.vaccinationmanager.model.service.BranchesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branches")
public class BranchesController {
    private final BranchesService branchesService;

    public BranchesController(BranchesService branchesService) {
        this.branchesService = branchesService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Branches>> findAll()
    {
        List<Branches> branchesList= branchesService.findAll();
        return new ResponseEntity<>(branchesList, HttpStatus.OK);
    }

    @GetMapping("/findByName/{name}")
    public ResponseEntity<Branches> findBranchesByName(@PathVariable("name") String name)
    {
        Branches branches= branchesService.findBranchByName(name);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Branches> findBranchesById(@PathVariable("id") Long id)
    {
        Branches branches= branchesService.findBranchById(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<Branches> saveBranches(@RequestBody Branches branches)
    {
        Branches newBranches= branchesService.saveBranch(branches);
        return new ResponseEntity<>(newBranches, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> saveBranches(@PathVariable("id") Long id)
    {
        branchesService.deleteBranch(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}