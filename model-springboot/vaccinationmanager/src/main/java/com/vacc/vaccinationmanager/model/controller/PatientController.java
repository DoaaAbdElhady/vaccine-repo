package com.vacc.vaccinationmanager.model.controller;

import com.vacc.vaccinationmanager.model.entity.Patient;
import com.vacc.vaccinationmanager.model.service.PatientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/patient")
public class PatientController {
    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Patient>> findAll()
    {
        List<Patient> patientList= patientService.findAll();
        return new ResponseEntity<>(patientList, HttpStatus.OK);
    }

    @GetMapping("/findByName/{name}")
    public ResponseEntity<Patient> findPatientByName(@PathVariable("name") String name)
    {
        Patient patient= patientService.findPatientByName(name);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Patient> findPatientByName(@PathVariable("id") Long id)
    {
        Patient patient= patientService.findPatientById(id);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<Patient> savePatient(@RequestBody Patient patient)
    {
        Patient newPatient= patientService.savePatient(patient);
        return new ResponseEntity<>(newPatient, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> savePatient(@PathVariable("id") Long id)
    {
        patientService.deletePatient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}