package com.vacc.vaccinationmanager.model.controller;
import com.vacc.vaccinationmanager.model.entity.PatientReservation; 
import com.vacc.vaccinationmanager.model.service.PatientReservationService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@RequestMapping("/patientreservation")
public class PatientReservationController {
    private final PatientReservationService patientReservationService;


    public PatientReservationController(PatientReservationService patientReservationService) {
        this.patientReservationService = patientReservationService;
    }


    @GetMapping("/findAll")
    public ResponseEntity<List<PatientReservation>> findAll()
    {
        List<PatientReservation> patientReservationList= patientReservationService.findAll();
        return new ResponseEntity<>(patientReservationList, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<PatientReservation> findPatientReservationById(@PathVariable("id") Long id)
    {
        PatientReservation branches= patientReservationService.findPatientReservationById(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }


    @GetMapping("/findPatientReservationByStatus/{statusId}")
    public ResponseEntity<List<PatientReservation>> findPatientReservationByStatus(@PathVariable("statusId") Long id)
    {
        List<PatientReservation> branches= patientReservationService.findPatientReservationByStatus(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
    @GetMapping("/findPatientReservationByBranchId/{branchId}")
    public ResponseEntity<List<PatientReservation>> findPatientReservationByBranchId(@PathVariable("branchId") Long id)
    {
        List<PatientReservation> branches= patientReservationService.findPatientReservationByBranchId(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
    @GetMapping("/findPatientReservationByVaccineId/{vaccineId}")
    public ResponseEntity<List<PatientReservation>> findPatientReservationByVaccineId(@PathVariable("vaccineId") Long id)
    {
        List<PatientReservation> branches= patientReservationService.findPatientReservationByVaccineId(id);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<PatientReservation> savePatientReservation(@RequestBody PatientReservation patientReservation)
    {
        PatientReservation newPatientReservation= patientReservationService.save(patientReservation);
        return new ResponseEntity<>(newPatientReservation, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> deletePatientReservation(@PathVariable("id") Long id)
    {
        patientReservationService.deletePatientReservationById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @GetMapping("/findPatientReservationByBranchIdAndStatusId/{branchId}/{statusId}")
    public ResponseEntity<List<PatientReservation>> findPatientReservationByBranchIdAndStatusId(@PathVariable("branchId") Long branchId,@PathVariable("statusId") Long statusId)
    {
        List<PatientReservation> branches= patientReservationService.findPatientReservationByBranchIdAndStatusId(branchId,statusId);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
    private Date convertStringToDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = null;
        try {
            parsed = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }
    @GetMapping("/findByReservationDate/{fromDate}/{statusId}")
    public ResponseEntity<List<PatientReservation>> findByReservationDate(@PathVariable("fromDate") String fromDate,@PathVariable("statusId") Long statusId)
    {
        java.sql.Date sqlDate = new java.sql.Date(convertStringToDate(fromDate).getTime());
        List<PatientReservation> branches= patientReservationService.findByReservationDate(sqlDate,statusId);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
    @GetMapping("/findByReservationDateBetween/{fromDate}/{toDate}/{statusId}")
    public ResponseEntity<List<PatientReservation>> findByReservationDateBetween(@PathVariable("fromDate")  String fromDate, @PathVariable("toDate") String toDate, @PathVariable("statusId") Long statusId)
    {
        java.sql.Date fromDateSQL = new java.sql.Date(convertStringToDate(fromDate).getTime());

        java.sql.Date toDateSQL = new java.sql.Date(convertStringToDate(toDate).getTime());
        List<PatientReservation> branches= patientReservationService.findByReservationDateBetween(fromDateSQL,toDateSQL,statusId);
        return new ResponseEntity<>(branches, HttpStatus.OK);
    }
}
