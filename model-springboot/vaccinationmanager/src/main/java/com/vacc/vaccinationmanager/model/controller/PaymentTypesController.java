package com.vacc.vaccinationmanager.model.controller;

import com.vacc.vaccinationmanager.model.entity.PaymentTypes;
import com.vacc.vaccinationmanager.model.service.PaymentTypesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paymentTypes")
public class PaymentTypesController {
    private final PaymentTypesService paymentTypesService;

    public PaymentTypesController(PaymentTypesService paymentTypesService) {
        this.paymentTypesService = paymentTypesService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<PaymentTypes>> findAll()
    {
        List<PaymentTypes> paymentTypesList= paymentTypesService.findAll();
        return new ResponseEntity<>(paymentTypesList, HttpStatus.OK);
    }
 

    @GetMapping("/findById/{id}")
    public ResponseEntity<PaymentTypes> findPaymentTypesByName(@PathVariable("id") Long id)
    {
        PaymentTypes paymentTypes= paymentTypesService.findPaymentTypesById(id);
        return new ResponseEntity<>(paymentTypes, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<PaymentTypes> savePaymentTypes(@RequestBody PaymentTypes paymentTypes)
    {
        PaymentTypes newPaymentTypes= paymentTypesService.savePaymentTypes(paymentTypes);
        return new ResponseEntity<>(newPaymentTypes, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> savePaymentTypes(@PathVariable("id") Long id)
    {
        paymentTypesService.deletePaymentTypes(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}