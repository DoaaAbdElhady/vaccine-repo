package com.vacc.vaccinationmanager.model.controller;

import com.vacc.vaccinationmanager.model.entity.Status;
import com.vacc.vaccinationmanager.model.service.StatusService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/status")
public class StatusController {
    private final StatusService statusService;

    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Status>> findAll()
    {
        List<Status> statusList= statusService.findAll();
        return new ResponseEntity<>(statusList, HttpStatus.OK);
    }

    @GetMapping("/findByName/{name}")
    public ResponseEntity<Status> findStatusByName(@PathVariable("name") String name)
    {
        Status status= statusService.findStatusByName(name);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Status> findStatusByName(@PathVariable("id") Long id)
    {
        Status status= statusService.findStatusById(id);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<Status> saveStatus(@RequestBody Status status)
    {
        Status newStatus= statusService.saveStatus(status);
        return new ResponseEntity<>(newStatus, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> saveStatus(@PathVariable("id") Long id)
    {
        statusService.deleteStatus(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}