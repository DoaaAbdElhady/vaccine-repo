package com.vacc.vaccinationmanager.model.controller;

import com.vacc.vaccinationmanager.model.entity.Vaccine;
import com.vacc.vaccinationmanager.model.service.VaccineService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vaccine")
public class VaccineController {
    private final VaccineService vaccineService;

    public VaccineController(VaccineService vaccineService) {
        this.vaccineService = vaccineService;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Vaccine>> findAll()
    {
        List<Vaccine> vaccineList= vaccineService.findAll();
        return new ResponseEntity<>(vaccineList, HttpStatus.OK);
    }

    @GetMapping("/findByName/{name}")
    public ResponseEntity<List<Vaccine>> findVaccineByName(@PathVariable("name") String name)
    {
        List<Vaccine> vaccineList= vaccineService.findVaccineByName(name);
        return new ResponseEntity<>(vaccineList, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<Vaccine> findVaccineById(@PathVariable("id") Long id)
    {
        Vaccine vaccine= vaccineService.findVaccineById(id);
        return new ResponseEntity<>(vaccine, HttpStatus.OK);
    }

    @PostMapping ("/save")
    public ResponseEntity<Vaccine> saveVaccine(@RequestBody Vaccine vaccine)
    {
        Vaccine newVaccine= vaccineService.saveVaccine(vaccine);
        return new ResponseEntity<>(newVaccine, HttpStatus.OK);
    }

    @PostMapping ("/delete/{id}")
    public ResponseEntity<?> saveVaccine(@PathVariable("id") Long id)
    {
        vaccineService.deleteVaccine(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}