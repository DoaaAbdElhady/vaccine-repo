package com.vacc.vaccinationmanager.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "vacc_branch_days")
public class BranchDays implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long brDayId;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="BRANCH_ID")
    private Branches branches;

    @ManyToOne
    @JoinColumn(name="WEEK_ID",nullable=true)
    private WeekDay weekDay;


    public BranchDays() {
    }

    public BranchDays(Long brDayId,  Branches branches) {
        this.brDayId = brDayId;
        this.branches = branches;
//        this.weekDay = weekDay;
    }

    @Override
    public String toString() {
        return "BranchDays{" +
                "id=" + brDayId +
                '}';
    }

    public Long getBrDayId() {
        return brDayId;
    }

    public void setBrDayId(Long brDayId) {
        this.brDayId = brDayId;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
    }
//    public Branches getBranches() {
//        return branches;
//    }
//
//    public void setBranches(Branches branches) {
//        this.branches = branches;
//    }
}
