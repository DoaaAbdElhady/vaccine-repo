package com.vacc.vaccinationmanager.model.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "vacc_branches_vaccines")
public class BranchVaccines implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
   // private Long branchId;
   //private Long vaccineId;
   // private Long statusId;
    private Date fromDate;
    private Date untilDate;
    @ManyToOne
    @JoinColumn(name="BRANCH_ID",nullable=true)
     private Branches branches;
    @ManyToOne
    @JoinColumn(name="VACCINE_ID",nullable=true)
    private Vaccine vaccine;
    @ManyToOne
    @JoinColumn(name="STATUS_ID",nullable=true)
    private Status status;


    public Branches getBranches() {
        return branches;
    }

    public void setBranches(Branches branches) {
        this.branches = branches;
    }

    public Vaccine getVaccine() {
        return vaccine;
    }

    public void setVaccine(Vaccine vaccine) {
        this.vaccine = vaccine;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }



    public BranchVaccines() {
    }

    public BranchVaccines(Long id,   Date fromDate, Date untilDate) {
        this.id = id;

        this.fromDate = fromDate;
        this.untilDate = untilDate;
    }

    @Override
    public String toString() {
        return "BranchVaccines{" +
                "id=" + id +
                ", fromDate=" + fromDate +
                ", untilDate=" + untilDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(Date untilDate) {
        this.untilDate = untilDate;
    }
}
