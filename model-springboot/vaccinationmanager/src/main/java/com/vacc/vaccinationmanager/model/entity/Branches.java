package com.vacc.vaccinationmanager.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "vacc_branches")
public class Branches implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private String address;
    private String phone;
    private String email;
    private Long workingDaysStart;
    private Long workingDaysEnd;
    private Long workingHoursStart;

    @OneToMany(mappedBy = "branches")
    private List<BranchDays> branchDays;




    private Long workingHoursEnd;


    public Branches() {
    }

    public Branches(Long id, String name, String address, String phone, String email, Long workingDaysStart, Long workingDaysEnd, Long workingHoursStart, List<BranchDays> branchDays, Long workingHoursEnd) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.workingDaysStart = workingDaysStart;
        this.workingDaysEnd = workingDaysEnd;
        this.workingHoursStart = workingHoursStart;
        this.branchDays = branchDays;
        this.workingHoursEnd = workingHoursEnd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getWorkingDaysStart() {
        return workingDaysStart;
    }

    public void setWorkingDaysStart(Long workingDaysStart) {
        this.workingDaysStart = workingDaysStart;
    }

    public Long getWorkingDaysEnd() {
        return workingDaysEnd;
    }

    public void setWorkingDaysEnd(Long workingDaysEnd) {
        this.workingDaysEnd = workingDaysEnd;
    }

    public Long getWorkingHoursStart() {
        return workingHoursStart;
    }

    public void setWorkingHoursStart(Long workingHoursStart) {
        this.workingHoursStart = workingHoursStart;
    }

    public Long getWorkingHoursEnd() {
        return workingHoursEnd;
    }

    public void setWorkingHoursEnd(Long workingHoursEnd) {
        this.workingHoursEnd = workingHoursEnd;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<BranchDays> getBranchDays() {
        return branchDays;
    }

    public void setBranchDays(List<BranchDays> branchDays) {
        this.branchDays = branchDays;
    }

    @Override
    public String toString() {
        return "Branches{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", workingDaysStart=" + workingDaysStart +
                ", workingDaysEnd=" + workingDaysEnd +
                ", workingHoursStart=" + workingHoursStart +
                ", workingHoursEnd=" + workingHoursEnd +
                '}';
    }
}
