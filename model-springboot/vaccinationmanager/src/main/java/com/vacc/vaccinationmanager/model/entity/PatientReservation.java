package com.vacc.vaccinationmanager.model.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;


@Entity
@Table(name = "vacc_patient_reservation")
public class PatientReservation implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private Date reservationDate;
    private Long reservationHour;
    private Long reservationMinute;

    @ManyToOne
    @JoinColumn(name="PATIENT_ID",nullable=true)
    private Patient patient;

    @ManyToOne
    @JoinColumn(name="BRANCH_VACCINE_ID",nullable=true)
    private BranchVaccines branchVaccines;

    @ManyToOne
    @JoinColumn(name="STATUS_ID",nullable=true)
    private Status status;

    @ManyToOne
    @JoinColumn(name="PAYMENT_ID",nullable=true)
    private PaymentTypes paymentTypes;

    public PatientReservation() {
    }

    public PatientReservation(Long id, Date reservationDate, Long reservationHour, Long reservationMinute, Patient patient, BranchVaccines branchVaccines, Status status, PaymentTypes paymentTypes) {
        this.id = id;
        this.reservationDate = reservationDate;
        this.reservationHour = reservationHour;
        this.reservationMinute = reservationMinute;
        this.patient = patient;
        this.branchVaccines = branchVaccines;
        this.status = status;
        this.paymentTypes = paymentTypes;
    }

    @Override
    public String toString() {
        return "PatientReservation{" +
                "id=" + id +
                ", reservationDate=" + reservationDate +
                ", reservationHour=" + reservationHour +
                ", reservationMinute=" + reservationMinute +
                ", patient=" + patient +
                ", branchVaccines=" + branchVaccines +
                ", status=" + status +
                ", paymentTypes=" + paymentTypes +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Long getReservationHour() {
        return reservationHour;
    }

    public void setReservationHour(Long reservationHour) {
        this.reservationHour = reservationHour;
    }

    public Long getReservationMinute() {
        return reservationMinute;
    }

    public void setReservationMinute(Long reservationMinute) {
        this.reservationMinute = reservationMinute;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public BranchVaccines getBranchVaccines() {
        return branchVaccines;
    }

    public void setBranchVaccines(BranchVaccines branchVaccines) {
        this.branchVaccines = branchVaccines;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PaymentTypes getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(PaymentTypes paymentTypes) {
        this.paymentTypes = paymentTypes;
    }
}
