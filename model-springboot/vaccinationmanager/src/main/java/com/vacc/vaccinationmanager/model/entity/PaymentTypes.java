package com.vacc.vaccinationmanager.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "vacc_paymet_types")
public class PaymentTypes implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    private String name;

    public PaymentTypes(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public PaymentTypes() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PaymentTypes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
