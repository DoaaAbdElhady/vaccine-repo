package com.vacc.vaccinationmanager.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= "vacc_vaccine")
public class Vaccine implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String name;
    private Long numberOfDosage;
    private String sideEffects;
    @Column(name = "EXPIRAION_PERIOD")
    private String expirationPeriod;
    private String storageCondition;

    public Vaccine() {}

    public Vaccine(Long id, String name, Long numberOfDosage, String sideEffects, String expirationPeriod, String storageCondition) {
        this.id = id;
        this.name = name;
        this.numberOfDosage = numberOfDosage;
        this.sideEffects = sideEffects;
        this.expirationPeriod = expirationPeriod;
        this.storageCondition = storageCondition;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumberOfDosage() {
        return numberOfDosage;
    }

    public void setNumberOfDosage(Long numberOfDosage) {
        this.numberOfDosage = numberOfDosage;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getExpirationPeriod() {
        return expirationPeriod;
    }

    public void setExpirationPeriod(String expirationPeriod) {
        this.expirationPeriod = expirationPeriod;
    }

    public String getStorageCondition() {
        return storageCondition;
    }

    public void setStorageCondition(String storageCondition) {
        this.storageCondition = storageCondition;
    }

    @Override
    public String toString() {
        return "Vaccine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numberOfDosage=" + numberOfDosage +
                ", sideEffects='" + sideEffects + '\'' +
                ", expirationPeriod='" + expirationPeriod + '\'' +
                ", storageCondition='" + storageCondition + '\'' +
                '}';
    }

}
