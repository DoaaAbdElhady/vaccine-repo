package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.BranchDays;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface BranchDaysRepository  extends JpaRepository<BranchDays, Long> {
}
