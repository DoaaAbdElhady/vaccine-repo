package com.vacc.vaccinationmanager.model.repository;
import com.vacc.vaccinationmanager.model.entity.BranchVaccines;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
public interface BranchVaccinesRepository  extends JpaRepository<BranchVaccines, Long>{
    Optional<List<BranchVaccines>> findBranchVaccinesByStatusId(Long status);
    Optional<List<BranchVaccines>> findBranchVaccineByBranchesId(Long branchId);
    Optional<List<BranchVaccines>> findBranchVaccineByVaccineId(Long vaccineId);
}
