package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.Branches;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;


public interface BranchesRepository extends JpaRepository<Branches, Long> {
    Optional<Branches> findBranchesByName(String name);
}
