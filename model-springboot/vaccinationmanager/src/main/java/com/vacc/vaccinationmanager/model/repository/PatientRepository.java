package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    Optional<Patient> findPatientByName(String name);
}
