package com.vacc.vaccinationmanager.model.repository;
import com.vacc.vaccinationmanager.model.entity.PatientReservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
public interface PatientReservationRepository  extends JpaRepository<PatientReservation, Long>{
    Optional<List<PatientReservation>> findPatientReservationByStatusId(Long status);
    Optional<List<PatientReservation>> findPatientReservationByReservationDateAndStatusId(Date reservationDate, Long statusId);
    Optional<List<PatientReservation>> findPatientReservationByReservationDateBetweenAndStatusId(Date reservationFromDate,Date reservationUntilDate, Long statusId);
    Optional<List<PatientReservation>> findPatientReservationByBranchVaccinesBranchesId(Long branchId);
    Optional<List<PatientReservation>> findPatientReservationByBranchVaccinesVaccineId(Long vaccineId);
    Optional<List<PatientReservation>> findPatientReservationByBranchVaccinesBranchesIdAndStatusId(Long branchId, Long statusId);
}
