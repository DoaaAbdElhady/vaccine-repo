package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.PaymentTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface PaymentTypesRepository  extends JpaRepository<PaymentTypes, Long> {
}
