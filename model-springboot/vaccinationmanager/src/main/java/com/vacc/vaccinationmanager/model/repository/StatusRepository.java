package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface StatusRepository  extends JpaRepository<Status, Long> {
    Optional<Status> findStatusByName(String name);
}
