package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.Vaccine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VaccineRepository extends JpaRepository<Vaccine, Long>{
    Optional<List<Vaccine>> findVaccineByName(String name);
}
