package com.vacc.vaccinationmanager.model.repository;

import com.vacc.vaccinationmanager.model.entity.WeekDay;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface WeekDayRepository  extends JpaRepository<WeekDay, Long> {
}
