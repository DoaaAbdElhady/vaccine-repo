package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.BranchDays;
import com.vacc.vaccinationmanager.model.repository.BranchDaysRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class BranchDaysService {

    private final BranchDaysRepository branchDaysRepository;

    @Autowired
    public BranchDaysService(BranchDaysRepository branchDaysRepository) {
        this.branchDaysRepository = branchDaysRepository;
    }
    public BranchDays saveBranchDays (BranchDays branchDays)
    {
        return branchDaysRepository.save(branchDays);
    }

    public void deleteBranchDays (Long id)
    {
         branchDaysRepository.deleteById(id);
    }

    public BranchDays findBranchDaysById (Long id)
    {
       return branchDaysRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }

    
    public List<BranchDays> findAll ()
    {
        return branchDaysRepository.findAll();
    }

}
