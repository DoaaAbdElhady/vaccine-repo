package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.BranchVaccines;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import com.vacc.vaccinationmanager.model.repository.BranchVaccinesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BranchVaccinesService {

    private final BranchVaccinesRepository branchVaccinesRepository;
    @Autowired
    public BranchVaccinesService(BranchVaccinesRepository branchVaccinesRepository) {
        this.branchVaccinesRepository = branchVaccinesRepository;
    }

    public BranchVaccines saveBranch (BranchVaccines branchVaccines)
    {
        return branchVaccinesRepository.save(branchVaccines);
    }

    public void deleteBranchVaccine (Long id)
    {
        branchVaccinesRepository.deleteById(id);
    }

    public BranchVaccines findBranchVaccineById (Long id)
    {
        return branchVaccinesRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }
    public List<BranchVaccines> findBranchVaccineByStatus(Long status)
    {
        return branchVaccinesRepository.findBranchVaccinesByStatusId(status).orElseThrow(() -> new NotFoundException("status: "+status+" not found."));
    }
    public List<BranchVaccines> findBranchVaccineByBranchId(Long branchId)
    {
        return branchVaccinesRepository.findBranchVaccineByBranchesId(branchId).orElseThrow(() -> new NotFoundException("branchId: "+branchId+" not found."));
    }
    public List<BranchVaccines> findBranchVaccineByVaccineId(Long vaccineId)
    {
        return branchVaccinesRepository.findBranchVaccineByVaccineId(vaccineId).orElseThrow(() -> new NotFoundException("vaccineId: "+vaccineId+" not found."));
    }

    public List<BranchVaccines> findAll ()
    {
        return branchVaccinesRepository.findAll();
    }

}
