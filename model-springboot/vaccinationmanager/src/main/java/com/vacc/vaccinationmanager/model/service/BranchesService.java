package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.Branches;
import com.vacc.vaccinationmanager.model.repository.BranchesRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BranchesService {

    private final BranchesRepository branchesRepository;

    @Autowired
    public BranchesService(BranchesRepository branchesRepository) {
        this.branchesRepository = branchesRepository;
    }
    public Branches saveBranch (Branches branches)
    {
        return branchesRepository.save(branches);
    }

    public void deleteBranch (Long id)
    {
         branchesRepository.deleteById(id);
    }

    public Branches findBranchById (Long id)
    {
       return branchesRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }

    public Branches findBranchByName (String name)
    {
        return branchesRepository.findBranchesByName(name).orElseThrow(() ->new NotFoundException("Name: "+name+" not found."));
    }

    public List<Branches> findAll ()
    {
        return branchesRepository.findAll();
    }

}
