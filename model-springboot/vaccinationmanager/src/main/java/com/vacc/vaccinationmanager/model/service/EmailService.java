package com.vacc.vaccinationmanager.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {


    @Autowired
    private JavaMailSender javaMailSender;
    public void sendMail(String to, String body, String topic){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("vaccinationmanager@gmail.com");
        simpleMailMessage.setSubject(topic);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setText(body);
        javaMailSender.send(simpleMailMessage);
    }
}
