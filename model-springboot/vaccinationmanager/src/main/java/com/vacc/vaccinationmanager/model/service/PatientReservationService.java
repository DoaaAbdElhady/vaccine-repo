package com.vacc.vaccinationmanager.model.service;
import com.vacc.vaccinationmanager.model.entity.Patient;
import com.vacc.vaccinationmanager.model.entity.PatientReservation;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import com.vacc.vaccinationmanager.model.repository.PatientRepository;
import com.vacc.vaccinationmanager.model.repository.PatientReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;


@Service
public class PatientReservationService {

    private final PatientReservationRepository patientReservationRepository;
    private final PatientRepository patientRepository;

    @Autowired
    private EmailService emailService;
    @Autowired
    public PatientReservationService(PatientReservationRepository patientReservationRepository, PatientRepository patientRepository) {
        this.patientReservationRepository = patientReservationRepository;
        this.patientRepository=patientRepository;
    }

    public PatientReservation save (PatientReservation patientReservation)
    {
        if(patientReservation.getStatus().getId()==1)
        {Patient patient = patientRepository.save(patientReservation.getPatient());
         patientReservation.getPatient().setId(patient.getId());}
        PatientReservation newPatientReservation=  patientReservationRepository.save(patientReservation);
        if(patientReservation.getStatus().getId()==1)
            emailService.sendMail(patientReservation.getPatient().getEmail(),"Dear "+patientReservation.getPatient().getName()+"<br> reservation Done.","Vaccine Reservation");
       else
            emailService.sendMail(patientReservation.getPatient().getEmail(),"Dear "+patientReservation.getPatient().getName()+"<br> vacination Done.","Vaccine Reservation");
        return newPatientReservation;
    }

    public void deletePatientReservationById (Long id)
    {
        patientReservationRepository.deleteById(id);
    }

    public PatientReservation findPatientReservationById (Long id)
    {
        return patientReservationRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }
    public List<PatientReservation> findPatientReservationByStatus(Long status)
    {
        return patientReservationRepository.findPatientReservationByStatusId(status).orElseThrow(() -> new NotFoundException("status: "+status+" not found."));
    }
    public List<PatientReservation> findPatientReservationByBranchId(Long branchId)
    {
        return patientReservationRepository.findPatientReservationByBranchVaccinesBranchesId(branchId).orElseThrow(() -> new NotFoundException("branchId: "+branchId+" not found."));
    }
    public List<PatientReservation> findPatientReservationByVaccineId(Long vaccineId)
    {
        return patientReservationRepository.findPatientReservationByBranchVaccinesVaccineId(vaccineId).orElseThrow(() -> new NotFoundException("vaccineId: "+vaccineId+" not found."));
    }

    public List<PatientReservation> findByReservationDate(Date reservationDate, Long statusId)
    {
        return patientReservationRepository.findPatientReservationByReservationDateAndStatusId(reservationDate,statusId).orElseThrow(() -> new NotFoundException("reservationDate not found."));
    }
    public List<PatientReservation> findByReservationDateBetween(Date reservationFromDate,Date reservationUntilDate, Long statusId)
    {
        return patientReservationRepository.findPatientReservationByReservationDateBetweenAndStatusId(reservationFromDate,reservationUntilDate,statusId).orElseThrow(() -> new NotFoundException("reservationDate not found."));
    }

    public List<PatientReservation> findAll ()
    {
        return patientReservationRepository.findAll();
    }

    public List<PatientReservation> findPatientReservationByBranchIdAndStatusId(Long branchId,Long statusId)
    {
        return patientReservationRepository.findPatientReservationByBranchVaccinesBranchesIdAndStatusId(branchId,statusId).orElseThrow(() -> new NotFoundException("branchId: "+branchId+" not found."));
    }
}
