package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.Patient;
import com.vacc.vaccinationmanager.model.repository.PatientRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }
    public Patient savePatient (Patient patient)
    {
        return patientRepository.save(patient);
    }

    public void deletePatient (Long id)
    {
         patientRepository.deleteById(id);
    }

    public Patient findPatientById (Long id)
    {
       return patientRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }

    public Patient findPatientByName (String name)
    {
        return patientRepository.findPatientByName(name).orElseThrow(() ->new NotFoundException("Name: "+name+" not found."));
    }

    public List<Patient> findAll ()
    {
        return patientRepository.findAll();
    }

}
