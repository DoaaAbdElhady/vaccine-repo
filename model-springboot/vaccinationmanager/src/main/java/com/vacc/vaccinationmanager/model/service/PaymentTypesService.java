package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.PaymentTypes;
import com.vacc.vaccinationmanager.model.repository.PaymentTypesRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class PaymentTypesService {

    private final PaymentTypesRepository statusRepository;

    @Autowired
    public PaymentTypesService(PaymentTypesRepository statusRepository) {
        this.statusRepository = statusRepository;
    }
    public PaymentTypes savePaymentTypes (PaymentTypes status)
    {
        return statusRepository.save(status);
    }

    public void deletePaymentTypes (Long id)
    {
         statusRepository.deleteById(id);
    }

    public PaymentTypes findPaymentTypesById (Long id)
    {
       return statusRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }


    public List<PaymentTypes> findAll ()
    {
        return statusRepository.findAll();
    }

}
