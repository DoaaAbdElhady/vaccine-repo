package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.Status;
import com.vacc.vaccinationmanager.model.repository.StatusRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class StatusService {

    private final StatusRepository statusRepository;

    @Autowired
    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }
    public Status saveStatus (Status status)
    {
        return statusRepository.save(status);
    }

    public void deleteStatus (Long id)
    {
         statusRepository.deleteById(id);
    }

    public Status findStatusById (Long id)
    {
       return statusRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }

    public Status findStatusByName (String name)
    {
        return statusRepository.findStatusByName(name).orElseThrow(() ->new NotFoundException("Name: "+name+" not found."));
    }

    public List<Status> findAll ()
    {
        return statusRepository.findAll();
    }

}
