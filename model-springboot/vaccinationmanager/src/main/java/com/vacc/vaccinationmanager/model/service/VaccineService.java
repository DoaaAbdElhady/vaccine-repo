package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.Vaccine;
import com.vacc.vaccinationmanager.model.repository.VaccineRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class VaccineService {

    private final VaccineRepository vaccineRepository;

    @Autowired
    public VaccineService(VaccineRepository vaccineRepository) {
        this.vaccineRepository = vaccineRepository;
    }

    public Vaccine saveVaccine (Vaccine vaccine)
    {
        return vaccineRepository.save(vaccine);
    }

    public void deleteVaccine (Long id)
    {
         vaccineRepository.deleteById(id);
    }

    public Vaccine findVaccineById (Long id)
    {
       return vaccineRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }

    public List<Vaccine> findVaccineByName (String name)
    {
        return vaccineRepository.findVaccineByName(name).orElseThrow(() ->new NotFoundException("Name: "+name+" not found."));
    }

    public List<Vaccine> findAll ()
    {
        return vaccineRepository.findAll();
    }

}
