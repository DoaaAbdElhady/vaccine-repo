package com.vacc.vaccinationmanager.model.service;

import com.vacc.vaccinationmanager.model.entity.WeekDay;
import com.vacc.vaccinationmanager.model.repository.WeekDayRepository;
import com.vacc.vaccinationmanager.model.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class WeekDayService {

    private final WeekDayRepository weekDayRepository;

    @Autowired
    public WeekDayService(WeekDayRepository weekDayRepository) {
        this.weekDayRepository = weekDayRepository;
    }
    public WeekDay saveWeekDay (WeekDay weekDay)
    {
        return weekDayRepository.save(weekDay);
    }

    public void deleteWeekDay (Long id)
    {
         weekDayRepository.deleteById(id);
    }

    public WeekDay findWeekDayById (Long id)
    {
       return weekDayRepository.findById(id).orElseThrow(() -> new NotFoundException("Id: "+id+" not found."));
    }

    
    public List<WeekDay> findAll ()
    {
        return weekDayRepository.findAll();
    }

}
