-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: vacc
-- ------------------------------------------------------
-- Server version	5.5.51

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vacc_branches`
--

DROP TABLE IF EXISTS `vacc_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacc_branches` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `WORKING_DAYS_START` int(11) DEFAULT NULL,
  `WORKING_DAYS_END` int(11) DEFAULT NULL,
  `WORKING_HOURS_START` int(11) DEFAULT NULL,
  `WORKING_HOURS_END` int(11) DEFAULT NULL,
  `PHONE` varchar(20) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacc_branches`
--

LOCK TABLES `vacc_branches` WRITE;
/*!40000 ALTER TABLE `vacc_branches` DISABLE KEYS */;
INSERT INTO `vacc_branches` VALUES (1,'The American University in Cairo','P.O. Box 74 New Cairo 11835, Egypt ',2,6,8,17,'20.2.2615.3515','caps@aucegypt.edu'),(2,'Cairo University','Giza',1,6,8,15,'0100005555','cairous@cu.com'),(3,'Alex University','Alex',2,6,9,17,'0122222225','alexun@au.com'),(4,'Central hospital','nasr city, Cairo',2,5,8,20,'+200012323','centrahjospital@gmail.com'),(5,'main hospital','new Cairo, Cairo',2,6,8,17,'+2909090000','xxxx@gmail.com'),(6,'branch 2','el giza, Giza',3,5,8,20,'+2899798798','xxxxx@yahoo.com');
/*!40000 ALTER TABLE `vacc_branches` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-14  8:23:32
