-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: vacc
-- ------------------------------------------------------
-- Server version	5.5.51

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vacc_patient_reservation`
--

DROP TABLE IF EXISTS `vacc_patient_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacc_patient_reservation` (
  `ID` int(11) NOT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `BRANCH_VACCINE_ID` int(11) DEFAULT NULL,
  `RESERVATION_DATE` date DEFAULT NULL,
  `RESERVATION_HOUR` int(11) DEFAULT NULL,
  `RESERVATION_MINUTE` int(11) DEFAULT NULL,
  `STATUS_ID` int(11) DEFAULT NULL,
  `PAYMENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `patient_reservation_fk1_idx` (`PATIENT_ID`),
  KEY `patient_reservation_fk2_idx` (`BRANCH_VACCINE_ID`),
  KEY `patient_reservation_fk2_idx1` (`STATUS_ID`),
  KEY `patient_reservation_fk4_idx` (`PAYMENT_ID`),
  CONSTRAINT `FKernmrv15hdvdm4ui30hy6q7ei` FOREIGN KEY (`PAYMENT_ID`) REFERENCES `vacc_paymet_types` (`ID`),
  CONSTRAINT `FKfy52g2bndyggv5trygy03y12c` FOREIGN KEY (`PATIENT_ID`) REFERENCES `vacc_patient` (`ID`),
  CONSTRAINT `FKqf9n9bb91kxujm2h36fay0nhy` FOREIGN KEY (`STATUS_ID`) REFERENCES `vacc_status` (`ID`),
  CONSTRAINT `FKrq4vq35b8fewuuqi2g03yf6na` FOREIGN KEY (`BRANCH_VACCINE_ID`) REFERENCES `vacc_branches_vaccines` (`ID`),
  CONSTRAINT `patient_reservation_fk1` FOREIGN KEY (`PATIENT_ID`) REFERENCES `vacc_patient` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `patient_reservation_fk2` FOREIGN KEY (`BRANCH_VACCINE_ID`) REFERENCES `vacc_branches_vaccines` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `patient_reservation_fk3` FOREIGN KEY (`STATUS_ID`) REFERENCES `vacc_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `patient_reservation_fk4` FOREIGN KEY (`PAYMENT_ID`) REFERENCES `vacc_paymet_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacc_patient_reservation`
--

LOCK TABLES `vacc_patient_reservation` WRITE;
/*!40000 ALTER TABLE `vacc_patient_reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacc_patient_reservation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-14  8:23:34
