import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VaccineComponent } from './vaccine/vaccine.component';
import { BranchComponent } from './branch/branch.component';
import { BranchvaccinesComponent } from './branchvaccines/branchvaccines.component';
import { BranchvaccinesbyvaccineComponent } from './branchvaccinesbyvaccine/branchvaccinesbyvaccine.component';
import { BranchvaccinesbybranchComponent } from './branchvaccinesbybranch/branchvaccinesbybranch.component';
import { PatientreservationComponent } from './patientreservation/patientreservation.component';
import { VacinationapproveComponent } from './vacinationapprove/vacinationapprove.component';
import { ReportsComponent } from './reports/reports.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { VaccineperbranchreportComponent } from './vaccineperbranchreport/vaccineperbranchreport.component';
import { VaccinebydatesreportComponent } from './vaccinebydatesreport/vaccinebydatesreport.component';

const routes: Routes = [
  { path: '', component: VaccineComponent },
  { path: 'vaccines', component: VaccineComponent },
  { path: 'branches', component: BranchComponent },
  { path: 'branchvaccines', component: BranchvaccinesComponent },
  { path: 'branchvaccinesbyvaccine/:id', component: BranchvaccinesbyvaccineComponent },
  { path: 'branchvaccinesbybranch/:id', component: BranchvaccinesbybranchComponent },
  { path: 'patientreservationComponent/:id', component: PatientreservationComponent },
  { path: 'vacinationapprove/:statusId', component: VacinationapproveComponent },
  { path: 'vaccineperbranchreport', component: VaccineperbranchreportComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'vaccinebydatesreport', component: VaccinebydatesreportComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [VaccineComponent, BranchComponent, BranchvaccinesComponent, BranchvaccinesbyvaccineComponent, BranchvaccinesbybranchComponent, PatientreservationComponent, VacinationapproveComponent, ReportsComponent, VaccineperbranchreportComponent, VaccinebydatesreportComponent]
