import { Component, OnInit } from '@angular/core';
import { Branch } from './branch';
import { BranchService } from './branch.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  public branches?: Branch[];
  constructor(private branchservice: BranchService, private router: Router) { }

  ngOnInit(): void {
    this.findAll();
  }

  public findAll(): void {
    this.branchservice.findAll().subscribe((branchList: Branch[]) => {
      this.branches = branchList;
    })
  }

  navigateToVaccineDetails(branchId: number): void {
    this.router.navigate(['/branchvaccinesbybranch/' +  branchId ]);
  }
}
