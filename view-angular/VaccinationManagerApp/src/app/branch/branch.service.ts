import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Branch } from '../branch/branch';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  private apiBaseUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient ) { }


  public findAll(): Observable<Branch[]> {
    return this.http.get<Branch[]>(this.apiBaseUrl+'/branches/findAll');
  }


  public findBranchByName(name: string): Observable<Branch[]> {
    return this.http.get<Branch[]>(this.apiBaseUrl +'/branches/findBranchByName/${name}');
  }

  public findBranchById(id: number): Observable<Branch> {
    return this.http.get<Branch>(this.apiBaseUrl +'/branches/findById/' + id);
  }

  public saveBranch(Branch: Branch): Observable<Branch> {
    return this.http.post<Branch>(this.apiBaseUrl +'/branches/save', Branch);
  }


  public deleteBranch(id: number): Observable<void> {
    return this.http.delete<void>(this.apiBaseUrl +'/branches/deleteBranch/${id}');
  }
}
