import { BranchDays } from './branchdays';

export interface Branch {
  id: number;
  name: string;
  address: string;
  phone: string;
  email: string;
  workingDaysStart: number;
  workingDaysEnd: number;
  workingHoursStart: number;
  workingHoursEnd: number;
  branchDays: BranchDays[];
}
