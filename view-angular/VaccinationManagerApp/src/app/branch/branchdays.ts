import { WeekDay } from './weekday';

export interface BranchDays {
  brDayId: number;
  weekDay: WeekDay;
}
