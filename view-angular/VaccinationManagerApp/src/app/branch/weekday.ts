export interface WeekDay {
  id: number;
  name: string;
}
