import { Component, OnInit } from '@angular/core';
import { Branchvaccines } from './branchvaccines';
import { BranchvaccinesService } from './branchvaccines.service';

@Component({
  selector: 'app-branchvaccines',
  templateUrl: './branchvaccines.component.html',
  styleUrls: ['./branchvaccines.component.css']
})
export class BranchvaccinesComponent implements OnInit {
  public branchvaccines?: Branchvaccines[];
  constructor(private branchvaccinesService: BranchvaccinesService) { }

  ngOnInit(): void {
    this.findAll();
  }

  public findAll(): void {
    this.branchvaccinesService.findAll().subscribe((branchvaccinesList: Branchvaccines[]) => {
      this.branchvaccines = branchvaccinesList;
    })
  }
}
