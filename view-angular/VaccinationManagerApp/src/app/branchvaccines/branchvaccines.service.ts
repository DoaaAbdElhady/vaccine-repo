import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { Branchvaccines } from './branchvaccines';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BranchvaccinesService {
  private apiBaseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Branchvaccines[]> {
    return this.http.get<Branchvaccines[]>(this.apiBaseUrl+'/branchesvaccines/findAll');
  }

  public findById(id: number): Observable<Branchvaccines> {
    return this.http.get<Branchvaccines>(this.apiBaseUrl +'/branchesvaccines/findById/'+id);
  }

  public findBranchVaccineByStatus(id: number): Observable<Branchvaccines[]> {
    return this.http.get<Branchvaccines[]>(this.apiBaseUrl +'/branchesvaccines/findBranchVaccinesById/${id}');
  }

  public findBranchVaccineByBranchId(id: number): Observable<Branchvaccines[]> {
    return this.http.get<Branchvaccines[]>(this.apiBaseUrl +'/branchesvaccines/findBranchVaccineByBranchId/' + id );
  }

  public findBranchVaccineByVaccineId(id: number): Observable<Branchvaccines[]> {
    return this.http.get<Branchvaccines[]>(this.apiBaseUrl +'/branchesvaccines/findBranchVaccineByVaccineId/'+id);
  }


  public saveBranchVaccines(Branchvaccines: Branchvaccines): Observable<Branchvaccines> {
    return this.http.post<Branchvaccines>(this.apiBaseUrl +'/branchesvaccines/save', Branchvaccines);
  }


  public delete(id: number): Observable<void> {
    return this.http.delete<void>(this.apiBaseUrl +'/branchesvaccines/delete/${id}');
  }
}
