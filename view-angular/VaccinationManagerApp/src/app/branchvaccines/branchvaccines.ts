
import { Branch } from '../branch/branch';
import { Vaccine } from '../vaccine/vaccine';

export interface Branchvaccines {
  id: number;
  untilDate: Date;
  fromDate: Date;
  branches: Branch;
  vaccine: Vaccine;

}
