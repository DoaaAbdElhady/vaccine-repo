import { Component, OnInit } from '@angular/core';
import { Branchvaccines } from '../branchvaccines/branchvaccines';
import { BranchvaccinesService } from '../branchvaccines/branchvaccines.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Branch } from '../branch/branch';
import { BranchService } from '../branch/branch.service';

@Component({
  selector: 'app-branchvaccinesbybranch',
  templateUrl: './branchvaccinesbybranch.component.html',
  styleUrls: ['./branchvaccinesbybranch.component.css']
})
export class BranchvaccinesbybranchComponent implements OnInit {
  public branch?: Branch;
  public branchvaccines?: Branchvaccines[];
  constructor(private branchvaccinesService: BranchvaccinesService, private branchservice: BranchService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    
    let id = parseInt("" + this.activatedRoute.snapshot.paramMap.get('id'));
    this.findBranchById(id);
    this.findBranchVaccineByBranchId(id);
  }

  public findBranchVaccineByBranchId(id: number): void {
    console.log(id);
    this.branchvaccinesService.findBranchVaccineByBranchId(id).subscribe((branchvaccinesList: Branchvaccines[]) => {
      this.branchvaccines = branchvaccinesList;
    })
  }

  public findBranchById(id: number): void {
    console.log(id);
    this.branchservice.findBranchById(id).subscribe((branch: Branch) => {
      this.branch = branch;
    })
  }

  public findAll(): void {
    this.branchvaccinesService.findAll().subscribe((branchvaccinesList: Branchvaccines[]) => {
      this.branchvaccines = branchvaccinesList;
    })
  }


  public navigateToReservation(branchvaccine: number): void {
    this.router.navigate(['/patientreservationComponent/' + branchvaccine  ]);
  }
}
