import { Component, OnInit } from '@angular/core';
import { Branchvaccines } from '../branchvaccines/branchvaccines';
import { BranchvaccinesService } from '../branchvaccines/branchvaccines.service';
import {  ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Vaccine } from '../vaccine/vaccine';
import { VaccineService } from '../vaccine/vaccine.Service';


@Component({
  selector: 'app-branchvaccinesbyvaccine',
  templateUrl: './branchvaccinesbyvaccine.component.html',
  styleUrls: ['./branchvaccinesbyvaccine.component.css']
})
export class BranchvaccinesbyvaccineComponent implements OnInit {
  public vaccine?: Vaccine;
  public branchvaccines?: Branchvaccines[];
  constructor(private branchvaccinesService: BranchvaccinesService, private vacineService: VaccineService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let id = parseInt("" + this.activatedRoute.snapshot.paramMap.get('id'));
    this.findVaccineById(id);
    this.findBranchVaccineByBranchId(id);
  }

  public findBranchVaccineByBranchId(id: number): void {

    console.log(id);
    this.branchvaccinesService.findBranchVaccineByVaccineId(id).subscribe((branchvaccinesList: Branchvaccines[]) => {
      this.branchvaccines = branchvaccinesList;
    })
  }

  public findVaccineById(id: number): void {
    this.vacineService.findVaccineById(id).subscribe((vaccine: Vaccine) => {
      this.vaccine = vaccine;
    })
  }
  public navigateToReservation(branchvaccine: number): void {
    this.router.navigate(['/patientreservationComponent/' + branchvaccine]);
  }
}
