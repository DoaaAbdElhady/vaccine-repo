export class Patient {
  id: number | undefined;
  name: string | undefined;
  address: string | undefined;
  phone: string | undefined;
  email: string | undefined;
}
