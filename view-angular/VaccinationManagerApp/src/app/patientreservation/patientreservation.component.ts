import { Component, OnInit, Input, ViewChild  } from '@angular/core';
import { BranchvaccinesService } from '../branchvaccines/branchvaccines.service';
import { PatientreservationService } from './patientreservation.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Branchvaccines } from '../branchvaccines/branchvaccines';
import { NgForm, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { PatientReservation } from './patientreservation';
import { HttpErrorResponse } from '@angular/common/http';
import { Patient } from './patient';
import { Status } from '../status';
import { PaymentTypes } from '../paymenttypes';
import { PaymentmethodService } from '../paymentmethod.service';

 

@Component({
  selector: 'app-patientreservation',
  templateUrl: './patientreservation.component.html',
  styleUrls: ['./patientreservation.component.css'] 
})
export class PatientreservationComponent implements OnInit {
 public patientreservation!: PatientReservation;
  public branchVaccine?: Branchvaccines;
  private patient!: Patient;
  private status!: Status;
  private paymentType!: PaymentTypes;
  date: any;
  public validBranchHours?: number[] = [];
  public validReservationMinutes?: number[] = [0,15,30,45];
  public paymentTypes?: PaymentTypes[];
  successAlert: boolean = false;

  constructor(private formBuilder: FormBuilder, private branchvaccinesService: BranchvaccinesService, private patientreservationService: PatientreservationService,
    private activatedRoute: ActivatedRoute, private router: Router, private paymentmethodService: PaymentmethodService) { }

  ngOnInit(): void {
    let id = parseInt("" + this.activatedRoute.snapshot.paramMap.get('id'));
    this.findAllPaymentTypes();
   this.findBranchVaccineId(id);
  }
  public findBranchVaccineId(id: number): void {
    this.patientreservation = new PatientReservation();
    this.branchvaccinesService.findById(id).subscribe((branchVaccine: Branchvaccines) => {
      this.branchVaccine = branchVaccine;
      var size = branchVaccine ?.branches ?.workingHoursEnd - branchVaccine ?.branches ?.workingHoursStart;
      for (let i = branchVaccine ?.branches ?.workingHoursStart; i < size + branchVaccine ?.branches ?.workingHoursStart; i++) {
        this.validBranchHours?.push(i);
      }
    })
  }


  reservationDate = new FormControl();
  reservationHour = new FormControl();
  reservationMinute = new FormControl();
  paymentMethodController = new FormControl();

  addForm = this.formBuilder.group({

    reservationDate: '',
    reservationHour: '',
    reservationMinute: '',
    paymentMethodController: ''
  })


  public reserveVaccine(addForm: NgForm): void { 
    this.patientreservation = addForm.value;
    console.log(this.paymentMethodController.value);
    this.patientreservation.reservationDate = this.reservationDate.value;
    this.patientreservation.reservationHour = this.reservationHour.value;
    this.patientreservation.reservationMinute = this.reservationMinute.value;
    this.status = new Status();
    this.status.id = 1;
    this.patientreservation.status = this.status;
    this.paymentType = new PaymentTypes();
    this.paymentType.id = this.paymentMethodController.value;
    this.patientreservation.paymentTypes = this.paymentType;
    this.patient = new Patient();
    this.patient.name = addForm.value.name;
    this.patient.email = addForm.value.email;
    this.patient.phone = addForm.value.phone;
    this.patient.address = addForm.value.address;
    this.patientreservation.patient = this.patient;
    this.patientreservation.branchVaccines = this.branchVaccine;
    this.patientreservationService.savePatientReservations(this.patientreservation).subscribe(
      (response: PatientReservation) => {
        console.log(response);
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
    this.successAlert = true;
    addForm.reset();
    this.reservationDate.reset();
    this.reservationHour.reset();
    this.reservationMinute.reset();
    this.paymentMethodController.reset();
  }


  closesuccessAlert() {
    this.successAlert = false;
  }

  myFilter: (date: Date | null) => boolean =
    (date: Date | null) => {
      
      const day = date ?.getDay();
  
      const validDates = this.branchVaccine ?.branches ?.branchDays.map(d => d.valueOf());
      if (date!=null)
      return (!validDates ?.includes(date.valueOf())) && day !== 6;
      else return true;
    }

  public findAllPaymentTypes(): void {
    this.paymentmethodService.findAll().subscribe((paymentTypes: PaymentTypes[]) => {
      this.paymentTypes = paymentTypes;
    })

  }
}
