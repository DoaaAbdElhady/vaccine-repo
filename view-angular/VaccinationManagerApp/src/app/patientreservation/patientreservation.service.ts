import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 
import { PatientReservation } from './patientreservation';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PatientreservationService {

  private apiBaseUrl = environment.apiBaseUrl;


  constructor(private http: HttpClient) { }

  public findAll(): Observable<PatientReservation[]> {
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findAll');
  }

  public findById(id: number): Observable<PatientReservation> {
    return this.http.get<PatientReservation>(this.apiBaseUrl+'/patientreservation/findById/'+id);
  }

  public findPatientReservationByStatus(id: number): Observable<PatientReservation[]> {
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findPatientReservationByStatus/'+id);
  }

  public findPatientReservationByBranchId(id: number): Observable<PatientReservation[]> {
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findPatientReservationByBranchId/' + id);
  }

  public findPatientReservationByVaccineId(id: number): Observable<PatientReservation[]> {
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findPatientReservationByVaccineId/' + id);
  }


  public savePatientReservations(patientReservation: PatientReservation): Observable<PatientReservation> {
    return this.http.post<PatientReservation>(this.apiBaseUrl+'/patientreservation/save',patientReservation);
  }


  public delete(id: number): Observable<void> {
    return this.http.delete<void>(this.apiBaseUrl+'/patientreservation/deletePatientReservation/'+id);
  }

  public findPatientReservationByBranchIdAndStatusId(branchId: number, statusId: number): Observable<PatientReservation[]> {
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findPatientReservationByBranchIdAndStatusId/' + branchId + '/' + statusId);
  }
 
  public findByReservationDate(fromDate: Date, statusId: number): Observable<PatientReservation[]> {
    let month = fromDate.getMonth() + 1;
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findByReservationDate/' + fromDate.getFullYear() + '-' + month + '-' + fromDate.getDate() + '/' + statusId);
  } 

  public findByReservationDateBetween(fromDate: Date, toDate: Date, statusId: number): Observable<PatientReservation[]> {

    let fromMonth = fromDate.getMonth() + 1;
    let toMonth = toDate.getMonth() + 1;
    return this.http.get<PatientReservation[]>(this.apiBaseUrl+'/patientreservation/findByReservationDateBetween/' + fromDate.getFullYear() + '-' + fromMonth + '-' + fromDate.getDate() + '/' + toDate.getFullYear() + '-' + toMonth + '-' + toDate.getDate()  + '/' + statusId);
  }
}
