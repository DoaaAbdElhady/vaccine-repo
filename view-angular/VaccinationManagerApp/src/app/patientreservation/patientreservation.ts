import { Data } from '@angular/router';
import { Patient } from './patient';
import { Branchvaccines } from '../branchvaccines/branchvaccines';
import { PaymentTypes } from '../paymenttypes';
import { Status } from '../status';

export class PatientReservation {
  id: number | undefined;
  reservationDate: Data | undefined;
  reservationHour: number | undefined;
  reservationMinute: number | undefined;
  patient: Patient | undefined;
  branchVaccines: Branchvaccines | undefined;
  status: Status | undefined;
  paymentTypes: PaymentTypes | undefined;


}
