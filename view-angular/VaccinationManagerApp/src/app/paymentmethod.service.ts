import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaymentTypes } from './paymenttypes';

@Injectable({
  providedIn: 'root'
})
export class PaymentmethodService {

  constructor(private http: HttpClient) { }

  public findAll(): Observable<PaymentTypes[]> {
    return this.http.get<PaymentTypes[]>('http://localhost:8080/paymentTypes/findAll');
  }
}
