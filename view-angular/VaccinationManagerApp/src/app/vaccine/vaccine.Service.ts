import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Vaccine} from '../vaccine/vaccine';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VaccineService {

  private apiBaseUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient  ) { }
  
  public findAll(): Observable<Vaccine[]>
 {
   return this.http.get<Vaccine[]>(this.apiBaseUrl+'/vaccine/findAll');
 }


  public findVaccineByName(name: string): Observable<Vaccine[]> {
    return this.http.get<Vaccine[]>(this.apiBaseUrl+'/vaccine/findVaccineByName/${name}');
  }

  public findVaccineById(id: number): Observable<Vaccine> {
    return this.http.get<Vaccine>(this.apiBaseUrl+'/vaccine/findById/' + id);
  }

  public saveVaccine(vaccine: Vaccine): Observable<Vaccine> {
    return this.http.post<Vaccine>(this.apiBaseUrl+'/vaccine/save', vaccine);
  }


  public deleteVaccine(id: number): Observable<void> {
    return this.http.delete<void>(this.apiBaseUrl+'/vaccine/deleteVaccine/${id}');
  }
}
