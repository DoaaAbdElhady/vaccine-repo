import { Component, OnInit } from '@angular/core';
import {VaccineService} from '../vaccine/vaccine.Service';
import { Vaccine } from '../vaccine/vaccine';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vaccine',
  templateUrl: './vaccine.component.html',
  styleUrls: ['./vaccine.component.css']
})
export class VaccineComponent implements OnInit {

  public vaccines?: Vaccine[];

  constructor(private vaccineService: VaccineService, private router: Router) { }

  ngOnInit(): void {
        this.findAll();
  }
  
  public findAll(): void {  
    this.vaccineService.findAll().subscribe((vaccineList: Vaccine[]) => {
    this.vaccines=vaccineList;
  })
  }

  navigateToVaccineDetails(vaccineId: number): void {
    this.router.navigate(['/branchvaccinesbyvaccine/' + vaccineId]);
  }


}
