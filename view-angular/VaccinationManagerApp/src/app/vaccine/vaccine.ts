export interface Vaccine {
id: number;
   name: string;
   numberOfDosage: number;
   sideEffects: string;
   expirationPeriod: string;
   storageCondition: string; 
}
