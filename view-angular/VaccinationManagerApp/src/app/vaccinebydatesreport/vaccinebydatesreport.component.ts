import { Component, OnInit, Input } from '@angular/core';
import { PatientreservationService } from '../patientreservation/patientreservation.service';
import { PatientReservation } from '../patientreservation/patientreservation';
import { ActivatedRoute } from '@angular/router';
import { BranchService } from '../branch/branch.service';
import { Branch } from '../branch/branch';
import { NgForm, FormGroup, FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms'; 

@Component({
  selector: 'app-vaccinebydatesreport',
  templateUrl: './vaccinebydatesreport.component.html',
  styleUrls: ['./vaccinebydatesreport.component.css']
})
export class VaccinebydatesreportComponent implements OnInit {

  @Input()
  reportType: any;

  public patientResevationList?: PatientReservation[];
  public branches?: Branch[];
  public date: any;
  reservationDateFrom = new FormControl();
  reservationDateTo = new FormControl();
  status = new FormControl();

  patientForm = this.formBuilder.group({
    reservationDateFrom: '',
    reservationDateTo: '',
    status: ''
  })


  constructor(private formBuilder: FormBuilder, private patientreservationService: PatientreservationService, private activatedRoute: ActivatedRoute, private branchservice: BranchService) { }

  ngOnInit(): void {
    this.findAllBranches();
  }

  public searchbyDate() {
    this.patientreservationService.findByReservationDate(this.reservationDateFrom.value, this.status.value).subscribe((patientResevationList: PatientReservation[]) => {
      this.patientResevationList = patientResevationList;
    })
  }

  public findByReservationDateBetween() {
    this.patientreservationService.findByReservationDateBetween(this.reservationDateFrom.value, this.reservationDateTo.value, this.status.value).subscribe((patientResevationList: PatientReservation[]) => {
      this.patientResevationList = patientResevationList;
    })
  }

  public findAllBranches(): void {
    this.branchservice.findAll().subscribe((branchList: Branch[]) => {
      this.branches = branchList;
    })

  }
}
