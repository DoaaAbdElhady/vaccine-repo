import { Component, OnInit } from '@angular/core';
import { PatientreservationService } from '../patientreservation/patientreservation.service';
import { PatientReservation } from '../patientreservation/patientreservation';
import { ActivatedRoute } from '@angular/router';
import { BranchService } from '../branch/branch.service';
import { Branch } from '../branch/branch';
import { NgForm, FormGroup, FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms'; 
@Component({
  selector: 'app-vaccineperbranchreport',
  templateUrl: './vaccineperbranchreport.component.html',
  styleUrls: ['./vaccineperbranchreport.component.css']
})
export class VaccineperbranchreportComponent implements OnInit {

  public patientResevationList?: PatientReservation[];
  public branches?: Branch[];

  branchIdController = new FormControl();
  reservationDate = new FormControl();

  patientForm = this.formBuilder.group({
    branchIdController: ''
  })


  constructor(private formBuilder: FormBuilder, private patientreservationService: PatientreservationService, private activatedRoute: ActivatedRoute, private branchservice: BranchService) { }

  ngOnInit(): void {
    this.findAllBranches();
  }
  
  public search() {
    console.log(this.branchIdController.value);
    this.patientreservationService.findPatientReservationByBranchIdAndStatusId(this.branchIdController.value, 2).subscribe((patientResevationList: PatientReservation[]) => {
      this.patientResevationList = patientResevationList;
    })

    console.log(this.patientResevationList);
  }

  public findAllBranches(): void {
    this.branchservice.findAll().subscribe((branchList: Branch[]) => {
      this.branches = branchList;
    })

  }
}
