import { Component, OnInit } from '@angular/core';
import { PatientreservationService } from '../patientreservation/patientreservation.service';
import { PatientReservation } from '../patientreservation/patientreservation';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-vacinationapprove',
  templateUrl: './vacinationapprove.component.html',
  styleUrls: ['./vacinationapprove.component.css']
})
export class VacinationapproveComponent implements OnInit {
  public patientResevationList?: PatientReservation[];
  successAlert: boolean = false;

  constructor(private patientreservationService: PatientreservationService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let statusId = parseInt("" + this.activatedRoute.snapshot.paramMap.get('statusId'));

    this.findPatientReservationByStatus(statusId);
  }

  public findPatientReservationByStatus(statusId: number): void {
    this.patientreservationService.findPatientReservationByStatus(statusId).subscribe((patientResevationList: PatientReservation[]) => {
      this.patientResevationList = patientResevationList;
    })
  }

  public approve(patientreservation: PatientReservation): void {
    if (patientreservation ?.status ?.id != null) { patientreservation.status.id = 2; }
      this.patientreservationService.savePatientReservations(patientreservation).subscribe(
      (response: PatientReservation) => {
        console.log(response);       
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
    this.findPatientReservationByStatus(1);
    this.successAlert = true; 
  }

  closesuccessAlert() {
    this.successAlert = false;
  }
}
